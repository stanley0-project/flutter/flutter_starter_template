// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_starter_template/common/cubits/themes/theme.cubit.dart';
import 'package:flutter_starter_template/features/home/cubit/home_cubit.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: ((context) => HomeCubit())),
      ],
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Test bloc"),
        ),
        body: BlocBuilder<HomeCubit, HomeState>(builder: (context, state) {
          return SingleChildScrollView(
            child: Column(
              children: [
                Card(
                  child: Row(
                    children: [
                      ElevatedButton(
                          onPressed: (() {
                            context.read<HomeCubit>().increment();
                            Navigator.of(context).pushNamed("error");
                          }),
                          child: const Text("Tester")),
                      Text('test ${state.number}'),
                      ElevatedButton(
                          onPressed: (() {
                            context.read<ThemeCubit>().setTheme("dark");
                          }),
                          child: const Text("Theme")),
                    ],
                  ),
                ),
              ],
            ),
          );
        }),
      ),
    );
  }
}
