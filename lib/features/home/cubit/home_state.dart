// ignore_for_file: public_member_api_docs, sort_constructors_first, must_be_immutable
part of 'home_cubit.dart';

@immutable
abstract class HomeState {
  int number;
  HomeState({
    required this.number,
  });
  HomeState clone() {
    return HomeInitial(number: number);
  }
}

class HomeInitial extends HomeState {
  HomeInitial({required super.number});
}
