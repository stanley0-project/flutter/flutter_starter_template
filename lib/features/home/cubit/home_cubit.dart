// ignore_for_file: depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeInitial(number: 0));

  void increment() {
    HomeState ns = state.clone();
    ns.number++;
    emit(ns);
  }
}
