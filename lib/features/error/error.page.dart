import 'package:flutter/material.dart';

class ErrorPage extends StatelessWidget {
  const ErrorPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Error"),
      ),
      body: Center(
        child: Text(
          "404",
          textScaleFactor: 1.5,
          style: TextStyle(color: Colors.grey[600]),
        ),
      ),
    );
  }
}
