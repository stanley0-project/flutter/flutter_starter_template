import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_starter_template/constants/theme.constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeCubit extends Cubit<ThemeData> {
  ThemeCubit() : super(defaultTheme) {
    getTheme();
  }

  static ThemeData defaultTheme = ThemeConstant().themeList()["light"];

  getTheme() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String color = preferences.getString("color") ?? "light";
    emit(ThemeConstant().themeList()[color]);
  }

  setTheme(String theme) async {
    SharedPreferences? preferences = await SharedPreferences.getInstance();

    preferences.setString("color", theme);
    emit(ThemeConstant().themeList()[theme]);
  }
}
