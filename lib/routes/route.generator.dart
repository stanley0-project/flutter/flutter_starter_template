import 'package:flutter/material.dart';
import 'package:flutter_starter_template/routes/route.dart';

class RouteGenerator {
  static Route generateRoute(RouteSettings settings) {
    return Routes.listOfRoute()[settings.name] ??
        Routes.listOfRoute()["error"]!;
  }
}
