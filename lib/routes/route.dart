import 'package:flutter/material.dart';
import 'package:flutter_starter_template/features/error/error.page.dart';
import 'package:flutter_starter_template/features/home/home.page.dart';

class Routes {
  static Map<String, PageRouteBuilder> listOfRoute() {
    return {
      "home": createRoute(const HomePage()),
      'error': createRoute(const ErrorPage()),
    };
  }

  static PageRouteBuilder createRoute(Widget page) {
    return PageRouteBuilder(
      pageBuilder: ((context, animation, secondaryAnimation) => page),
      transitionsBuilder: ((context, animation, secondaryAnimation, child) {
        const begin = Offset(0.0, 1.0);
        const end = Offset.zero;
        final tween = Tween(begin: begin, end: end);
        return SlideTransition(
          position: tween.animate(CurvedAnimation(
            parent: animation,
            curve: Curves.ease,
          )),
          child: child,
        );
      }),
    );
  }
}
