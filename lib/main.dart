import 'package:flutter/material.dart';
import 'package:flutter_starter_template/app.dart';
import 'package:flutter_starter_template/common/cubits/themes/theme.cubit.dart';

void main() async {
  runApp(const MyApp());
}
