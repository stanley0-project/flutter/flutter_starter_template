import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_starter_template/common/cubits/themes/theme.cubit.dart';
import 'package:flutter_starter_template/routes/route.generator.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ThemeCubit>(
      create: (context) => ThemeCubit(),
      child: BlocBuilder<ThemeCubit, ThemeData>(
        builder: (context, state) {
          return FutureBuilder(
              future: ThemeCubit().getTheme(),
              builder: (context, snapshot) {
                return (snapshot.connectionState == ConnectionState.done)
                    ? MaterialApp(
                        title: 'Flutter Demo',
                        debugShowCheckedModeBanner: false,
                        theme: state,
                        // theme: ThemeConstant().themeList()["dark"],
                        // darkTheme: ThemeConstant().themeList()["light"],
                        // themeMode: ThemeMode.system,
                        initialRoute: "home",
                        onGenerateRoute: ((settings) =>
                            RouteGenerator.generateRoute(settings)),
                      )
                    : Center();
              });
        },
      ),
    );
  }
}
