import 'package:flutter/material.dart';

class DarkTheme {
  static ThemeData get() {
    return ThemeData(
      brightness: Brightness.dark,
      primarySwatch: Colors.pink,
    );
  }
}
