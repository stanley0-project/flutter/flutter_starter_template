import 'package:flutter/material.dart';
import 'package:flutter_starter_template/themes/dark.theme.dart';
import 'package:flutter_starter_template/themes/light.theme.dart';

class CustomTheme {
  ThemeData lightTheme() {
    return LightTheme.get();
  }

  ThemeData darkTheme() {
    return DarkTheme.get();
  }
}
