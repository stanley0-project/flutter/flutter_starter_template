import 'package:flutter/material.dart';

class LightTheme {
  static ThemeData get() {
    return ThemeData(
      brightness: Brightness.light,
      primarySwatch: Colors.pink,
    );
  }
}
