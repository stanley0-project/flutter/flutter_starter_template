import 'package:flutter_starter_template/themes/dark.theme.dart';
import 'package:flutter_starter_template/themes/light.theme.dart';

class ThemeConstant {
  Map themeList() {
    return {
      "dark": DarkTheme.get(),
      "light": LightTheme.get(),
    };
  }
}
